const collectCoverageFrom = ['src/**/*.ts']

const coverageDirectory = 'coverage'

const coverageReporters = ['text']

const globals = {
  'ts-jest': {
    enableTsDiagnostics: true,
    tsConfigFile: 'tsconfig.jest.json'
  }
}

const moduleDirectories = ['node_modules']

const moduleFileExtensions = ['ts', 'js', 'json']

const moduleNameMapper = {
  '@logger': '<rootDir>/src/logger',
  '^@core/(.*)': '<rootDir>/src/core/$1'
}

const transform = {
  '^.+\\.ts$': 'ts-jest'
}

const testPathIgnorePatterns = ['/node_modules/']

const testRegex = '(/__tests__/.*|(\\.|/)(test|spec))\\.ts$'

module.exports = {
  collectCoverageFrom,
  coverageDirectory,
  coverageReporters,
  globals,
  moduleDirectories,
  moduleFileExtensions,
  moduleNameMapper,
  transform,
  testPathIgnorePatterns,
  testRegex
}
