const path = require('path')
const webpack = require('webpack')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const nodeExternals = require('webpack-node-externals')

const mode = 'development'

const entry = [
  'webpack/hot/poll?1000',
  './src/entry/development'
]

const buildModules = {
  rules: [
    {
      test: /\.ts$/,
      loader: 'ts-loader',
      exclude: /node_modules/,
      options: {
        transpileOnly: true
      }
    }
  ]
}

const resolve = {
  extensions: ['.ts', '.js'],
  alias: {
    '@core': path.resolve(__dirname, '../../src/core'),
    '@logger': path.resolve(__dirname, '../../src/logger')
  }
}

const output = {
  path: path.resolve(__dirname, '../../dist'),
  filename: 'server.js'
}

const plugins = [
  new ForkTsCheckerWebpackPlugin({tslint: path.resolve(__dirname, '../../tslint.json')}),
  new webpack.HotModuleReplacementPlugin({quiet: true}),
  new webpack.NamedModulesPlugin(),
  new webpack.NoEmitOnErrorsPlugin()
]

const devtool = 'inline-source-map'

const target = 'node'

const externals = [nodeExternals({whitelist: ['webpack/hot/poll?1000']})]

module.exports = {
  mode,
  entry,
  module: buildModules,
  resolve,
  output,
  plugins,
  devtool,
  target,
  externals
}
