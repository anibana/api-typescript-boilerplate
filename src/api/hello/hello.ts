import {toString} from 'fp-ts/lib/function'
import {ask, fromFuture} from '@core/monad/readerFuture'
import {ok, respondDefaultError,  Response} from '@core/response/response'
import {debug, error, info, warn} from '@logger'
import {Person} from './person'
import validate from './validator'
import {Person as ExternalPerson} from '../external/person'

const getFullName = ({name}: Person) =>
  ask().chain(({service}) => fromFuture(service.getName(name)))

const greet = ({firstName, lastName}: ExternalPerson): string => `Hello ${firstName} ${lastName}`

const withFeelings = (greeting: string) => ({greeting: `${greeting}!`})

export default (person: Person) =>
  getFullName(person)
    .chain(info())
    .map(greet)
    .map(withFeelings)
    .fold(respondDefaultError, ok)
