import {Person} from './person'
import {ok, respondDefaultError} from '@core/response/response'
import {getName, getNameFails} from './api.fixture'
import hello from './hello'

describe('#hello', () => {
  const person: Person = {name: 'John'}

  it('generates greeting from external call', async () => {
    const response = await hello(person).run(getName).promise()

    expect(response).toEqual(ok({greeting: 'Hello John Smith!'}))
  })

  it('responds default error if backend call fails', async () => {
    const response = await hello(person).run(getNameFails).promise()

    expect(response).toEqual(respondDefaultError())
  })
})
