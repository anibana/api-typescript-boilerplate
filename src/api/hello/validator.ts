import {Request} from '@core/request'
import validator from '@core/validator'
import {Person} from './person'

export const requestConverter = ({query: {name}}: Request) => ({name})

export default validator(Person, requestConverter)
