import {Future} from 'fluture'
import fakeLogger from '@core/logger/logger.fixture'

const successful = (name: string) => Future.of({firstName: name, lastName: 'Smith'})
const failing = (name: string) => Future.reject(null)

export const getName = {
  service: {getName: successful},
  logger: fakeLogger
}

export const getNameFails = {
  service: {getName: failing},
  logger: fakeLogger
}
