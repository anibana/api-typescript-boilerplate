import {readerFuture} from '@core/monad/readerFuture'
import Request from '@core/request'
import fakeLogger from '@core/logger/logger.fixture'
import {Person} from './person'
import {badRequest, ok} from '@core/response/response'
import {ValidationError} from '@core/errors'
import validate from './validator'

describe('#validator', () => {
  const deps = {logger: fakeLogger}
  const api = (person: Person) => readerFuture.of(ok({greeting: 'greeting'}))

  it('responds badRequest if name is missing from the request', async () => {
    const request = Request({}, {})
    const response = await validate(api)(request).run(deps).promise()

    expect(response).toEqual(badRequest(
      ValidationError({name: 'should be of type: string'})
    ))
  })

  it('calls api if name is present from the request', async () => {
    const request = Request({}, {name: 'John'})
    const response = await validate(api)(request).run(deps).promise()

    expect(response).toEqual(ok({greeting: 'greeting'}))
  })
})
