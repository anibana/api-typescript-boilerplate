import * as t from 'io-ts'

export const Person = t.type({
  name: t.string
})

export type Person = t.TypeOf<typeof Person>
