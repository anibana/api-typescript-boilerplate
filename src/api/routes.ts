import {routes} from '@core/router/routes'
import {GET} from '@core/router/methods'

import getName from './external/getName'
import hello from './hello'

export default routes([
  GET('/external/getName/:name')(getName),
  GET('/hello')(hello)
])
