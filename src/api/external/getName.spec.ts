import Request from '@core/request'
import getName from './getName'

describe('#getName', () => {
  it('returns result with firstName from params and lastName as "Smith"', async () => {
    const name = 'John'
    const response = await getName(Request({name})).run({}).promise()

    expect(response.body.firstName).toEqual('John')
    expect(response.body.lastName).toEqual('Smith')
  })
})
