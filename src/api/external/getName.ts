import {ReaderFuture, readerFuture} from '@core/monad/readerFuture'
import {Request} from '@core/request'
import {ok, Response} from '@core/response/response'

export default ({params: {name}}: Request): ReaderFuture<any, any, Response> =>
  readerFuture.of(ok({
    firstName: name,
    lastName: 'Smith'
  }))
