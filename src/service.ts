import axios from 'axios'
import {Config} from './config'
import {encaseP, Future} from 'fluture'
import {Person} from './api/external/person'

/* INFO: Axios is just for demonstration purposes only.
 * We can use isomorphic-fetch or some other simpler library as http client */
const getName = (config: Config) => (name: string): Promise<Person> =>
  axios.get(`${config.getNameUrl}/${name}`)
    .then(({data}) => data)

export interface Service {
  getName: (name: string) => Future<any, Person>
}

export const service = (config: Config): Service => ({
  getName: encaseP(getName(config))
})
