import * as http from 'http'

// to make HMR working when reloading server side application
let server: http.Server

const createServer = (middleware: any): void => {
  if (!server) {
    server = http.createServer(middleware)
  }
}

const startServer = (middleware: any, port: number): void => {
  createServer(middleware)
  server.listen(port)
}

const restartServer = (middleware: any, port: number): void => {
  server.close(() => {
    server = null
    startServer(middleware, port)
  })
}

const load = (middleware: any, port: number): void => {
  if (server) {
    return restartServer(middleware, port)
  }

  return startServer(middleware, port)
}

const on = (e: string, f: () => void): http.Server => server.on(e, f)

export default {
  load,
  on
}
