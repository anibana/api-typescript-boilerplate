import {ServerError, ValidationError} from './errors'

describe('Errors', () => {
  describe('#ValidationError', () => {
    it('constructs Error', () => {
      const errors = 'foo error'

      expect(ValidationError(errors)).toEqual({
        msg: 'There are validation errors',
        code: 'validationError',
        errors
      })
    })
  })

  describe('#ServerError', () => {
    it('constructs Error', () => {
      expect(ServerError('foo')).toEqual({
        msg: 'foo',
        code: 'internalServerError'
      })
    })

    it('uses default message if msg is not supplied', () => {
      expect(ServerError()).toEqual({
        msg: 'Something went wrong',
        code: 'internalServerError'
      })
    })
  })
})
