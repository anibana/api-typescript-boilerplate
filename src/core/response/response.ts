import HttpStatus from 'http-status-codes'
import {Request} from '../request'
import {ServerError} from '../errors'

export interface Response {
  body?: any
  status: number
}

type StatusCodes = number

const status: (status: StatusCodes) => (body?: any) => Response =
  (status) => (body?) => ({
    body,
    status
  })

export const badRequest = status(HttpStatus.BAD_REQUEST)
export const internalServerError = status(HttpStatus.INTERNAL_SERVER_ERROR)
export const notFound = status(HttpStatus.NOT_FOUND)
export const ok = status(HttpStatus.OK)
export const unauthorized = status(HttpStatus.UNAUTHORIZED)

export const respondDefaultError = (_?: any) => internalServerError(ServerError())
