import HttpStatus from 'http-status-codes'

import {
  badRequest,
  internalServerError,
  notFound,
  ok,
  unauthorized
} from './response'

const body = {foo: 'bar'}

describe('#badRequest', () => {
  const response = badRequest(body)

  it('constructs response with status BAD_REQUEST', () => {
    expect(response.status).toBe(HttpStatus.BAD_REQUEST)
    expect(response.body).toBe(body)
  })
})

describe('#internalServerError', () => {
  const response = internalServerError(body)

  it('constructs response with status INTERNAL_SERVER_ERROR', () => {
    expect(response.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR)
    expect(response.body).toBe(body)
  })
})

describe('#notFound', () => {
  const response = notFound(body)

  it('constructs response with status NOT_FOUND', () => {
    expect(response.status).toBe(HttpStatus.NOT_FOUND)
    expect(response.body).toBe(body)
  })
})

describe('#ok', () => {
  const response = ok(body)

  it('constructs response with status OK', () => {
    expect(response.status).toBe(HttpStatus.OK)
    expect(response.body).toBe(body)
  })
})

describe('#unauthorized', () => {
  const response = unauthorized(body)

  it('constructs response with status UNAUTHORIZED', () => {
    expect(response.status).toBe(HttpStatus.UNAUTHORIZED)
    expect(response.body).toBe(body)
  })
})
