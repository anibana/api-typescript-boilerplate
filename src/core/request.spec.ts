import {request} from './request'

describe('#request', () => {
  it('constructs Request from koa context', () => {
    const params = {foo: 'bar'}
    const query = {bar: 'baz'}
    const header = {'X-Men': 'First class'}
    const body = {some: 'body to love'}

    const ctx = {
      params,
      query,
      request: {header, body}
    }

    const result = request(ctx)

    expect(result.body).toBe(body)
    expect(result.headers).toBe(header)
    expect(result.params).toBe(params)
    expect(result.query).toBe(query)
  })
})
