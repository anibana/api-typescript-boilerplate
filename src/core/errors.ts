export const ValidationError = (errors: any) => ({
  msg: 'There are validation errors',
  code: 'validationError',
  errors
})

const errorMessage = 'Something went wrong'

export const ServerError = (msg = errorMessage) => ({
  msg,
  code: 'internalServerError'
})
