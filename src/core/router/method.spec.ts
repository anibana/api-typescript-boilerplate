import {readerFuture} from '@core/monad/readerFuture'
import {Request} from '@core/request'
import {ok} from '@core/response/response'
import {DELETE, GET, PATCH, POST, PUT} from './methods'

describe('Router methods', () => {
  const api = (request: Request) => readerFuture.of(ok('foo'))
  const path = '/foo'

  describe('#GET', () => {
    it('constructs Method with method="get"', () => {
      const apiMethod = GET(path)(api)

      expect(apiMethod).toEqual({
        api,
        method: 'get',
        path
      })
    })
  })

  describe('#POST', () => {
    it('constructs Method with method="post"', () => {
      const apiMethod = POST(path)(api)

      expect(apiMethod).toEqual({
        api,
        method: 'post',
        path
      })
    })
  })

  describe('#DELETE', () => {
    it('constructs Method with method="delete"', () => {
      const apiMethod = DELETE(path)(api)

      expect(apiMethod).toEqual({
        api,
        method: 'delete',
        path
      })
    })
  })

  describe('#PUT', () => {
    it('constructs Method with method="put"', () => {
      const apiMethod = PUT(path)(api)

      expect(apiMethod).toEqual({
        api,
        method: 'put',
        path
      })
    })
  })

  describe('#PATCH', () => {
    it('constructs Method with method="patch"', () => {
      const apiMethod = PATCH(path)(api)

      expect(apiMethod).toEqual({
        api,
        method: 'patch',
        path
      })
    })
  })
})
