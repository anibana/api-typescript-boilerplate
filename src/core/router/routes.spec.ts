import {ask, readerFuture, ReaderFuture} from '../monad/readerFuture'
import {routes} from './routes'
import {GET} from './methods'
import {default as request, Request} from '../request'
import {internalServerError, ok} from '../response/response'
import {fakeMiddleware} from './routes.fixture'

describe('#routes', () => {
  it('creates a router that maps path and params', async () => {
    const m = fakeMiddleware()
    const router = routes([GET('/')(getStuff)]).run({
      middleware: m,
      hello: (text: string) => `hello ${text}`
    })

    expect(router.routes().get['/']).toBeDefined()
    expect(await router.routes().get['/'](request({id: 'world'}))).toEqual(ok('hello world'))
  })

  const perform = ({params: {id}}: any) =>
    ask().chain(({hello}) => readerFuture.of(hello(id)))

  const getStuff = (request: Request) =>
    perform(request)
      .fold(internalServerError, ok)
})
