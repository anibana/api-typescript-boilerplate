import {ReaderFuture} from '../monad/readerFuture'
import {Response} from '../response/response'
import {Request} from '../request'
import {Api} from '../api'

export enum Types {
  DELETE = 'delete',
  GET = 'get',
  POST = 'post',
  PUT = 'put',
  PATCH = 'patch'
}

export interface Method {
  api: Api,
  method: Types,
  path: string
}

const method = (method: Types) => (path: string) => (api: Api): Method => ({
  api,
  method,
  path
})

export const GET = method(Types.GET)

export const POST = method(Types.POST)

export const DELETE = method(Types.DELETE)

export const PUT = method(Types.PUT)

export const PATCH = method(Types.PATCH)
