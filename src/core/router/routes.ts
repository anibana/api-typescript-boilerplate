import {reduce} from 'ramda'
import {ask} from 'fp-ts/lib/Reader'
import {Method} from './methods'
import {Response} from '../response/response'

export interface Router {
  register: (route: Method) => void
  routes: () => any
}

const register = (router: Router, route: Method) => {
  router.register(route)
  return router
}

export const routes = <D extends Middleware>(routes: Method[]) =>
  ask().map((dependencies: D) =>
    reduce(
      register,
      dependencies.middleware.createRouter(dependencies),
      routes
    )
  )

interface Middleware {
  middleware: {
    createRouter: (deps: any) => Router
  }
}
