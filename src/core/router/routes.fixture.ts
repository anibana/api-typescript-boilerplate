import {Router} from './routes'
import {Method} from './methods'
import {Request} from '../request'

export const fakeMiddleware = <D>() => ({
  createRouter: (dependencies: D) => fakeRouter<D>(dependencies)
})

export const fakeRouter = <D>(dependencies: D): Router => {
  const r: any = {
    get: {},
    post: {},
    put: {},
    delete: {},
    patch: {}
  }

  const evaluatable = (route: Method) => (request: Request) => route.api(request).run(dependencies).promise()

  return {
    register: (route) => {
      r[route.method][route.path] = evaluatable(route)
    },
    routes: ()  => r
  }
}
