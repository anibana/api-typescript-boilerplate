import Koa from 'koa'
import uuid from 'uuid'
import * as KoaRouter from 'koa-router'
import {reduce} from 'ramda'
import {Api} from './api'
import {Context} from './context'
import {request} from './request'
import {Response} from './response/response'
import {Router} from './router/routes'
import {Method} from './router/methods'
import {Config} from '../config'
import {default as logger, Logger} from './logger/logger'
import {Level, logLevels} from './logger/level'
import {info, logResponse} from '@logger'

export interface Middleware<D> {
  createRouter: (dependencies: D) => Router
  createLogger: (ctx: Context) => Logger
}

export const middleware = <D>({correlationKey}: Config): Middleware<D> => ({
  createRouter,
  createLogger: createLogger(correlationKey)
})

export const createLogger = (key: string) => (ctx: Context) => {
  const correlationId = ctx.request.header[key] || 'NO SESSION ID'
  const sessionId = uuid()

  return reduce((log: any, level: Level) =>
    ({...log, [level]: logger[level](correlationId, sessionId)}),
    {},
    logLevels
  )
}

export const createRouter = <D>(dependencies: D): Router => {
  const r = new KoaRouter()
  const depsWithLogger = withLogger(dependencies)
  const respond = createResponse(depsWithLogger)
  const register = (route: Method) => r[route.method](route.path, respond(route.api))

  return {
    register,
    routes: () => r.routes()
  }
}

export const createResponse = (dependencies: any) => (api: Api) => (ctx: Context) =>
  api(request(ctx))
    .chain(info(logResponse))
    .map(handleResponse(ctx))
    .run(dependencies(ctx))
    .promise()

const withLogger = (dependencies: any) => (ctx: Context) => ({
  ...dependencies,
  logger: dependencies.middleware.createLogger(ctx)
})

const handleResponse = (ctx: Context) => ({status, body}: Response): void => {
  ctx.status = status
  if (body) {
    ctx.body = body
  }
}
