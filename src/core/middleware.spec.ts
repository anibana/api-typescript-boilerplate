import Koa from 'koa'
import {Future} from 'fluture'
import {readerFuture} from './monad/readerFuture'
import {createResponse} from './middleware'
import {ok} from './response/response'
import {Context} from './context'
import fakeLogger from './logger/logger.fixture'

describe('koa middleware', () => {
  const response = createResponse((_: Context) => ({logger: fakeLogger}))

  it('responds to status ok', async () => {
    const ctx = createCtx()
    await response(statusOk)(ctx)

    expect(ctx.body).toEqual('foo')
    expect(ctx.status).toEqual(200)
  })

  const statusOk = () => readerFuture.of(ok('foo'))

  const createCtx = (): Context => ({
    request: {header: {}},
    params: {},
    query: {}
  })
})
