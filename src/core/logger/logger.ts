import * as winston from 'winston'
import {filter, getSemigroup, log, Logger} from 'logging-ts'
import {Future} from 'fluture'
import {future} from 'fp-ts-fluture/lib/Future'
import {Level} from './level'
import prodLogger from './production.logger'
import localLogger from './debug.logger'

interface Entry {
  message: string
  level: Level
  correlationId: string
  sessionId: string
}

const fileLogger = (logger: winston.LoggerInstance) => new Logger(
  ({level, message, correlationId, sessionId}: Entry) => new Future((_, res) => {
    logger[level](`[${correlationId}] - [${sessionId}] - ${message}`)
    res(undefined)
  })
)

const filterFuture = filter(future)
const debugLogger = filterFuture(fileLogger(localLogger), e => e.level === Level.DEBUG)
const productionLogger = filterFuture(fileLogger(prodLogger), e => e.level !== Level.DEBUG)

const logger = getSemigroup(future)<Entry>().concat(debugLogger, productionLogger)
const logFuture = log(logger)

const loggerOf = (level: Level) => (correlationId: string, sessionId: string) => (a: any, message: string) =>
  logFuture({message, level, correlationId, sessionId}).map(() => a)

const info = loggerOf(Level.INFO)
const warn = loggerOf(Level.WARN)
const debug = loggerOf(Level.DEBUG)
const error = loggerOf(Level.ERROR)

export interface Logger {
  info: any
  debug: any
  warn: any
  error: any
}

export default {
  info,
  warn,
  debug,
  error
}
