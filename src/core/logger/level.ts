export enum Level {
  INFO = 'info',
  ERROR = 'error',
  DEBUG = 'debug',
  WARN = 'warn'
}

export const logLevels: Level[] = [
  Level.INFO,
  Level.ERROR,
  Level.DEBUG,
  Level.WARN
]
