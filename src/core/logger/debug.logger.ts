import * as winston from 'winston'
import {Level} from './level'

export default new (winston.Logger)({
  level: process.env.logLevel || Level.DEBUG,
  transports: [
    new (winston.transports.Console)({
      timestamp: true,
      colorize: true
    })
  ]
})
