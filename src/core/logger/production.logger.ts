import * as winston from 'winston'
import path from 'path'
import fs from 'fs'
import {Level} from './level'

const logDir = 'logs'

let transports: any[] = [
  new (winston.transports.Console)({
    timestamp: true,
    colorize: true
  })
]

if (process.env.NODE_ENV === 'production') {
  if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir)
  }

  transports = [
    ...transports,
    new (winston.transports.File)({
      name: 'Info Transport',
      filename: path.join(logDir, 'info.log')
    }),
    new (winston.transports.File)({
      name: 'Error Transport',
      filename: path.join(logDir, 'error.log'),
      level: Level.ERROR
    })
  ]

}

const logger = new winston.Logger({
  level: process.env.logLevel || Level.INFO,
  transports
})

export default logger
