import {Future} from 'fluture'
import {toString} from 'fp-ts/lib/function'
import {Level} from '@core/logger/level'

const loggerOf = (level: Level) => Future.of

export default {
  [Level.DEBUG]: loggerOf(Level.DEBUG),
  [Level.ERROR]: loggerOf(Level.ERROR),
  [Level.INFO]: loggerOf(Level.INFO),
  [Level.WARN]: loggerOf(Level.WARN)
}
