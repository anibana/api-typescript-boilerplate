import {Context} from './context'

export interface Request {
  body?: any
  headers: any
  params: any
  query: any
}

export const request: (ctx: Context) => Request =
  ({params, query, request}) => ({
    body: request.body,
    headers: request.header,
    params,
    query
  })

export default (params?: any, query?: any, body?: any, headers?: any): Request => request({
  params,
  query,
  request: {
    body,
    header: headers
  }
})
