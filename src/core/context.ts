export interface Context {
  params?: any
  query?: any
  request: {
    body?: any
    header: any
  }
  status?: number
  body?: any
}
