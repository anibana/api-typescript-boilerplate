import {Request} from './request'
import {Response} from './response/response'
import {ReaderFuture} from './monad/readerFuture'

export type Api = (request: Request) => ReaderFuture<any, any, Response>
