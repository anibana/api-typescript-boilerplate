import {InterfaceType, ValidationError as Error} from 'io-ts'
import {ReaderFuture, readerFuture} from './monad/readerFuture'
import {Request} from './request'
import {badRequest} from './response/response'
import {info} from '@logger'
import {ValidationError} from './errors'
import {reduce} from 'ramda'

/* TODO: Refactor this or remove io-ts (because it is not fit to our requirement) */
const getMessage = (errors: Error[]) =>
  reduce((errorMessages: any, {context, value}: Error) => {
    const {key, type} = context[1]
    return {
      ...errorMessages,
      [key]: `should be of type: ${type.name}`
    }
  }, {}, errors)

const errorResult = (errors: Error[]) => {
  return readerFuture.of(
    badRequest(ValidationError(getMessage(errors)))
  ).chain(info())
}

/* tslint:disable: max-line-length */
export default <A>(schema: InterfaceType<any>, converter: (request: Request) => any) => (api: (r: A) => ReaderFuture<any, any, any>) =>
    (request: Request) =>
      schema.decode(converter(request))
        .fold(errorResult, api)
