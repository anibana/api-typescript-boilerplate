/* tslint:disable:
 * no-shadowed-variable
 * member-access
 * align
 * whitespace
 * no-unused-expression
 * one-variable-per-declaration
 * no-angle-bracket-type-assertion
 * object-literal-sort-keys
 */
import * as readerT from 'fp-ts/lib/ReaderT'
import Future from 'fluture'
import {future} from 'fp-ts-fluture/lib/Future'
import {Monad3} from 'fp-ts/lib/Monad'
import {Bifunctor3} from 'fp-ts/lib/Bifunctor'
import {Reader} from 'fp-ts/lib/Reader'

const readerTFuture = readerT.getReaderT(future)

declare module 'fp-ts/lib/HKT' {
  /* tslint:disable-next-line: interface-name */
  interface URI2HKT3<U, L, A> {
    ReaderFuture: ReaderFuture<U, L, A>
  }
}

export const URI = 'ReaderFuture'

export type URI = typeof URI

export class ReaderFuture<E, L, A> {
  readonly _A!: A
  readonly _L!: L
  readonly _U!: E
  readonly _URI!: URI

  /* tslint:disable-next-line: no-empty */
  constructor(readonly run: (e: E) => Future<L, A>) {}

  map<B>(f: (a: A) => B): ReaderFuture<E, L, B> {
    return new ReaderFuture(readerTFuture.map(f, this.run))
  }

  bimap<V, B>(f: (l: L) => V, g: (a: A) => B): ReaderFuture<E, V, B> {
    return new ReaderFuture((e: E) => this.run(e).bimap(f, g))
  }

  fold<V, B>(f: (l: L) => B, g: (a: A) => B): ReaderFuture<E, L, B> {
    return new ReaderFuture((e: E) => this.run(e).fold(f, g))
  }

  of<E, B>(b: B): ReaderFuture<E, L, B> {
    return of(b)
  }

  ap<B>(fab: ReaderFuture<E, L, (a: A) => B>): ReaderFuture<E, L, B> {
    return new ReaderFuture(readerTFuture.ap(fab.run, this.run))
  }

  ap_<B, C>(this: ReaderFuture<E, L, (b: B) => C>, fb: ReaderFuture<E, L, B>): ReaderFuture<E, L, C> {
    return fb.ap(this)
  }

  chain<B>(f: (a: A) => ReaderFuture<E, L, B>): ReaderFuture<E, L, B> {
    return new ReaderFuture(readerTFuture.chain(a => f(a).run, this.run))
  }
}

const map = <E, L, A, B>(fa: ReaderFuture<E, L, A>, f: (a: A) => B): ReaderFuture<E, L, B> => {
  return fa.map(f)
}

const bimap = <E, L, V, A, B>(fa: ReaderFuture<E, L, A>, f: (l: L) => V, g: (a: A) => B): ReaderFuture<E, V, B> => {
  return fa.bimap(f, g)
}

const of = <E, L, A>(a: A): ReaderFuture<E, L, A> => {
  return new ReaderFuture(readerTFuture.of(a))
}

const ap = <E, L, A, B>(fab: ReaderFuture<E, L, (a: A) => B>, fa: ReaderFuture<E, L, A>): ReaderFuture<E, L, B> => {
  return fa.ap(fab)
}

const chain = <E, L, A, B>(fa: ReaderFuture<E, L, A>, f: (a: A) => ReaderFuture<E, L, B>): ReaderFuture<E, L, B> => {
  return fa.chain(f)
}

const readerTask = readerT.ask(future)
export const ask = <E, L>(): ReaderFuture<E, L, E> => {
  return new ReaderFuture(readerTask())
}

const readerTasks = readerT.asks(future)
export const asks = <E, L, A>(f: (e: E) => A): ReaderFuture<E, L, A> => {
  return new ReaderFuture(readerTasks(f))
}

export const local = <E>(f: (e: E) => E) => <L, A>(fa: ReaderFuture<E, L, A>): ReaderFuture<E, L, A> => {
  return new ReaderFuture(e => fa.run(f(e)))
}

export const fromFuture = <E, L, A>(fa: Future<L, A>): ReaderFuture<E, L, A> => {
  return new ReaderFuture(() => fa)
}

export const reject = <E, L, A>(l: L): ReaderFuture<E, L, A> => {
  return fromFuture(Future.reject(l))
}

const readerTfromReader = readerT.fromReader(future)
export const fromReader = <E, L, A>(fa: Reader<E, A>): ReaderFuture<E, L, A> => {
  return new ReaderFuture(readerTfromReader(fa))
}

export const readerFuture: Monad3<URI> & Bifunctor3<URI> = {
  URI,
  map,
  bimap,
  of,
  ap,
  chain
}
