type Port = number | string

export interface Config {
  port: Port
  host: string
  getNameUrl: string
  correlationKey: string
}

const config: Config = {
  port: process.env.port || 3000,
  host: process.env.host || '0.0.0.0',
  getNameUrl: 'http://localhost:3000/external/getName',
  correlationKey: 'x-session-id'
}

export default config
