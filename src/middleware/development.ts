import * as Koa from 'koa'
import * as bodyparser from 'koa-bodyparser'
import * as cors from 'kcors'
import * as morgan from 'koa-morgan'
import {Dependencies} from '../dependencies'
import api from '../api/routes'

export default (dependencies: Dependencies) => {
  const app = new Koa()

  app.use(morgan(':date[iso] :method :status :url :response-time ms - :res[content-length]'))
  app.use(bodyparser())
  app.use(cors())
  app.use(api.run(dependencies).routes())

  return app.callback()
}
