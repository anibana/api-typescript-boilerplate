/* tslint:disable:no-console */
import '../config'
import '../middleware/development'
import '../dependencies'
import server from '../server'

const loadApp = (middleware: any, port: number) => {
  server.load(middleware, port)
  server.on('listening', () => {
    console.log(`server started on port ${port}`)
  })
}

const loadDeps = ([{default: middleware}, {default: config}, {default: dependencies}]: any[]) =>
  loadApp(middleware(dependencies(config)), config.port)

Promise.all([
  import('../middleware/development'),
  import('../config'),
  import('../dependencies')
]).then(loadDeps)
  .catch(err => console.log(err))

module.hot.accept()
