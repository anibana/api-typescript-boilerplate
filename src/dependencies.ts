import {Config} from './config'
import {middleware, Middleware} from './core/middleware'
import {Logger} from './core/logger/logger'
import {service, Service} from './service'

export interface Dependencies {
  middleware: Middleware<Dependencies>
  service: Service
}

export default (config: Config): Dependencies => ({
  middleware: middleware(config),
  service: service(config)
})
