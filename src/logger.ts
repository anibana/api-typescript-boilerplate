import {toString} from 'fp-ts/lib/function'
import {ask, fromFuture} from '@core/monad/readerFuture'
import {Level} from '@core/logger/level'
import {Response} from '@core/response/response'

const log = <A>(level: Level, a: A, message: string) =>
  ask().chain(({logger}) => fromFuture(logger[level](a, message)))

const defaultFormat = <A>(a: A) => toString(a)

const loggerOf = <A>(level: Level) => (format: (a: A) => string = defaultFormat) =>
  (a: A) => log(level, a, format(a))

export const logResponse = (response: Response) => `RESPONSE: ${toString(response)}`

export const debug = loggerOf(Level.DEBUG)
export const error = loggerOf(Level.ERROR)
export const info = loggerOf(Level.INFO)
export const warn = loggerOf(Level.WARN)
