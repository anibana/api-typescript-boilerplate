import {debug, error, info, warn} from './logger'
import {readerFuture} from '@core/monad/readerFuture'
import fakeLogger from '@core/logger/logger.fixture'

describe('Logger', () => {
  describe('#debug', () => {
    it('chains and should not modify the content of readerFuture', async () => {
      const content = 'foo'
      const afterLogging = readerFuture.of(content).chain(debug()).run({logger: fakeLogger}).promise()

      expect(await afterLogging).toEqual(content)
    })
  })

  describe('#error', () => {
    it('chains and should not modify the content of readerFuture', async () => {
      const content = 'foo'
      const afterLogging = readerFuture.of(content).chain(error()).run({logger: fakeLogger}).promise()

      expect(await afterLogging).toEqual(content)
    })
  })

  describe('#info', () => {
    it('chains and should not modify the content of readerFuture', async () => {
      const content = 'foo'
      const afterLogging = readerFuture.of(content).chain(info()).run({logger: fakeLogger}).promise()

      expect(await afterLogging).toEqual(content)
    })
  })

  describe('#warn', () => {
    it('chains and should not modify the content of readerFuture', async () => {
      const content = 'foo'
      const afterLogging = readerFuture.of(content).chain(warn()).run({logger: fakeLogger}).promise()

      expect(await afterLogging).toEqual(content)
    })
  })

})
